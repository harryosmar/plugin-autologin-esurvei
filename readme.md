# Plugin Auto Login Esurvei

Composer plugin for Project Survey. Url Generator For Autologin.

## Requirements

- PHP >= 5.5
- composer

## Installation

- Add this composer.json file

```
{
    "require": {
        "harryosmar/plugin-autologin-esurvei": "^1.0"
    },
    "scripts": {
        "post-install-cmd": [
            "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters"
        ],
        "post-update-cmd": [
            "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters"
        ]
    },
    "extra": {
        "incenteev-parameters": {
            "file": "config/autologinesurvei.yml",
            "dist-file": "vendor/harryosmar/plugin-autologin-esurvei/config/autologinesurvei.yml.dist",
            "parameter-key": "parameters"
        }
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:harryosmar/plugin-autologin-esurvei.git"
        }
    ]
}
```

- Then run
```
composer install harryosmar/plugin-autologin-esurvei
```

## How To Use
```php
<?php
/** @var \PluginAutoLoginEsurvei\Contract\UrlGenerator $urlGenerator */
$urlGenerator = \PluginAutoLoginEsurvei\ServiceContainer::getInstance()->getContainer()->get('autologinesurvei.urlgenerator');

// generate url front iptek
$urlIptek = $urlGenerator->generateUrlIptek('username', 'password');

// generate url front industri
$urlIndustri = $urlGenerator->generateUrlIndustri('username', 'password', 'id-lembaga');

// generate url admin
$urlAdmin = $urlGenerator->generateUrlAdmin('username', 'password');
```

## How to run test

```
composer test
```