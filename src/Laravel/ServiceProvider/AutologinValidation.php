<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/26/18
 * Time: 3:18 PM
 */

namespace PluginAutoLoginEsurvei\Laravel\ServiceProvider;

use Illuminate\Support\ServiceProvider;
use PluginAutoLoginEsurvei\ServiceContainer;
use PluginAutoLoginEsurvei\UrlGenerator;
use Validator;
use function PluginAutoLoginEsurvei\helper\Token\generateSignature;

class AutologinValidation extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /** @var UrlGenerator $urlGenerator */
        $urlGenerator = ServiceContainer::getInstance()->getContainer()->get('autologinesurvei.urlgenerator');

        /** @var \Illuminate\Validation\Validator $validator */
        Validator::extend('autologinsignature', function ($attribute, $value, $parameters, $validator) use ($urlGenerator) {
            $data = collect($validator->getData());
            return $value === generateSignature(
                    $data->get('client_id', 0),
                    $urlGenerator->getClientSecret(),
                    $data->get('timestamp', 0),
                    urldecode($data->get('username', '')),
                    urldecode($data->get('password', ''))
                );
        });
    }
}