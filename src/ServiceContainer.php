<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/26/18
 * Time: 4:38 PM
 */

namespace PluginAutoLoginEsurvei;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ServiceContainer
{
    /**
     * @var self
     */
    private static $instance;


    /**
     * @var ContainerBuilder
     */
    private $container;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * ServiceContainer constructor.
     */
    private function __construct()
    {
        $this->container = new ContainerBuilder();

        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__ . '/../services/'));
        $loader->load('services.yml');
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }
}