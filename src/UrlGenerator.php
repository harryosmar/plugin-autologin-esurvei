<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/25/18
 * Time: 10:00 AM
 */

namespace PluginAutoLoginEsurvei;

use PhpCollection\Map;
use function PluginAutoLoginEsurvei\helper\Token\generateSignature;

class UrlGenerator implements \PluginAutoLoginEsurvei\Contract\UrlGenerator
{

    /**
     * @var string
     */
    private $urlFront;

    /**
     * @var string
     */
    private $urlAdmin;

    /**
     * @var int
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * Base constructor.
     * @param string $urlFront
     * @param string $urlAdmin
     * @param int $clientId
     * @param string $clientSecret
     */
    public function __construct(
        $urlFront,
        $urlAdmin,
        $clientId,
        $clientSecret
    ) {
        $this->urlFront = $urlFront;
        $this->urlAdmin = $urlAdmin;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * @param string $username
     * @param string $password
     * @param int|null $timestamp
     * @return mixed
     */
    public function generateUrlIptek($username, $password, $timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        return $this->generateUrl(
            $this->urlFront,
            new Map(['username' => $username, 'password' => $password]),
            $timestamp
        );
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $idLembaga
     * @param int|null $timestamp
     * @return mixed
     */
    public function generateUrlIndustri($username, $password, $idLembaga, $timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        return $this->generateUrl(
            $this->urlFront,
            new Map(['username' => $username, 'password' => $password, 'id_lembaga' => $idLembaga]),
            $timestamp
        );
    }

    /**
     * @param string $username
     * @param string $password
     * @param int|null $timestamp
     * @return mixed
     */
    public function generateUrlAdmin($username, $password, $timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        return $this->generateUrl(
            $this->urlAdmin,
            new Map(['username' => $username, 'password' => $password]),
            $timestamp
        );
    }

    /**
     * @param $url
     * @param Map $parameters
     * @param int $timestamp
     * @return string
     */
    private function generateUrl($url, Map $parameters, $timestamp)
    {
        $completeParameters = array_merge(
            $parameters->all(),
            [
                'client_id' => $this->clientId,
                'timestamp' => $timestamp,
                'signature' => generateSignature(
                    $this->clientId,
                    $this->clientSecret,
                    $timestamp,
                    $parameters->get('username')->getOrElse(''),
                    $parameters->get('password')->getOrElse('')
                )
            ]
        );

        return sprintf(
            '%s?%s',
            $url,
            http_build_query($completeParameters)
        );
    }

    /**
     * @return string
     */
    public function getUrlFront(): string
    {
        return $this->urlFront;
    }

    /**
     * @return string
     */
    public function getUrlAdmin(): string
    {
        return $this->urlAdmin;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }
}