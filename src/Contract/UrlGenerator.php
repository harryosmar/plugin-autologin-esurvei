<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/25/18
 * Time: 9:55 AM
 */

namespace PluginAutoLoginEsurvei\Contract;

interface UrlGenerator
{
    /**
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function generateUrlIptek($username, $password);

    /**
     * @param string $username
     * @param string $password
     * @param string $idLembaga
     * @return mixed
     */
    public function generateUrlIndustri($username, $password, $idLembaga);

    /**
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function generateUrlAdmin($username, $password);
}