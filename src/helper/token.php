<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/25/18
 * Time: 10:18 AM
 */

namespace PluginAutoLoginEsurvei\helper\Token;

/**
 * @param int $clientId
 * @param string $clientSecret
 * @param integer $timestamp
 * @param string $username
 * @param string $password
 * @return string
 */
function generateSignature($clientId, $clientSecret, $timestamp, $username, $password)
{
    return str_replace(
        ['+','/','='],
        ['-','_',''],
        base64_encode(hash(
            'sha512',
            sprintf(
                '%d%s%d%s%s',
                $clientId,
                $clientSecret,
                $timestamp,
                $username,
                $password
            ),
            true
        ))
    );
}

