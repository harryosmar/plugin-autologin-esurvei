<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/26/18
 * Time: 4:18 PM
 */

namespace PluginAutoLoginEsurvei\Tests\unit;

use function PluginAutoLoginEsurvei\helper\Token\generateSignature;

class TokenTest extends Base
{
    public function test_generate_signature()
    {
        $this->assertEquals('g3Sak0Pr7IIryhbgPBXlFbyzGSN_XEiDRkyEKAs600B6xyidWrMJR2iTfRCykR_Ow-oqpdP4x5WWYXlBxuVnYQ', generateSignature(1, 'secret', 1535275163, 'username@domain.com', 'password'));
    }
}